# Gunpla - comment peindre un kit à l'aérographe

## 1. Avant propos

1. L'auteur peut avoir des opinion très prononcées. Tout insolence du type "je suis en désaccord avec toi" se verra très probablement sanctionner avec de la violence en le justifiant probablement avec beaucoup de mauvaise foi. ... Et par "probablement", j'entends une probabilité de : 100%, je sort le crique, la bombe lacrymogène, le pied de biche,..

1. L'auteur n'a aucun partenariat avec aucune des marques citées dans ce document, même si le voudrais bien parce qu'il aime la moula.

1. Certains produits présenté dans ce document peuvent ne pas être disponible dans tout les pays, et inversement l'auteur ne peut que présenter des produit disponible sur son lieu de résidence.

1.Certaines spécifications peuvent volontairement être plus haute que ce que conseiller habituellement, particulièrement en ce qui concerne la sécurité.

## Introduction

//TODO

## Nettoyage de l'aérographe

On commence par apprendre à nettoyer son aérographe, avant de l'utiliser. Tout comme l'enseigne Miyagi Sensei.

Pour une première fois il est intéressant de mettre une goute de peinture et une autre de diluant dans le godet, de remuer légèrement pour en mettre un peu partout, avant d'essayer de peindre pour la première.

### Matériel à préparer

- Des gants (voir la section "protection > gants").
- Du diluant.
- un chiffon, en tissu, ou un rouleau de sopalin, mais surtout pas un mouchoir ou tout autre matière qui pourrait s'effriter en le frottant quand il est humide.
- un conteur, type pot de peinture. (voir section "Matériel pour commencer à peindre > Autre outils utiles)
- un kit de nettoyage pour aérographe. Parfois fournis avec celui ci.

> warning : kit de nettoyage bas de gamme
>
> LEs kit de nettoyage fournis avec des aérographe bas de gamme sont, sans surprise, eux aussi bas de gamme. Ils maltraiterons ton aérographe sur le moyen termes. Ils très utiles d'utilisé un kit de nettoyage d'un marque de peinture ou d'aérographe, par exemple celui de Tamiya.

### Nettoyage rapide

A faire entre chaque changement de couleur.

1. On renverse son godet dans son pot si nécessaire.
1. Dans son pot, on spray ce qu'il pourrait resté dans l'aérographe.
1, Dans le godet, on verse le diluant du même type que celui utilisé pour diluer la dernière couleur utilisée. Le fond du godet est suffisant.
1, On imprègne son chiffon avec un peu de diluant puis l'on frotte les bord intérieur du godet.
1. On spray dans son pot jusqu'a vider totalement le diluant.
1. On répète le cycle jusqu'à ce que le diluant en sortie soit propre.

### Nettoyage complet

A faire absolument à chaque fin de session, pour préserver son aérographe le plus longtemps possible.

Dans un premier temps, il faudra réaliser un nettoyage rapide. Ensuite on démonte complètement son aérographe, comme spécifié dans le manuel de celui ci. Chaque pièce, et particulièrement l'aiguille, devront être nettoyé avoir un produit nettoyant pour aérographe.

// produit alternatif ?

> :ballot_box_with_check: Produit de nettoyage
> toute les marques de peinture pour hobby propose leur produit de nettoyage, souvent sous un nom évocateur contenant les mots "nettoyant" ou "cleaner".

> :warning: : Le produit de nettoyage doit être utiliser avec des gants adapté. Certains plastique fondent à cause de celui-ci.

> :warning: Le produit de nettoyage est corrosif pour la plupart des plastique. A ne pas poser sur un kit, ou pour nettoyer certaines cabine de peinture en plastic.

> :warning: Le produit de nettoyage est aussi, dans une moindre mesure, corrosif  pour l'aérographe. L'on peut nettoyer son aérographe, mais le laisser tremper dedans pendant plusieurs heure.

### Comment ne pas abîmer son aiguille lors d'un démontage.

La méthode tiens du bon sens, on veux protéger le côté pointu de l'aiguille. La méthode suivante et à adapter selon l'aérographe, mais reste globalement la même.

1. On va d'abord chercher à accéder à l'arrière de l'aiguille, en démontant les pièces autour autant que possible.
2. Ensuite on va démonter la buse avec l'outil dédié fournis avec l'aérographe.
3. Une cela fait, on va poussé l'aiguille de l'arrière vers l'avant.
4. lors du remontage on passera l'aiguille par l'avant de l'aérographe.

## Les protections

Commme toutes activités impliquant des produits potentiellement nocif pour la santé, un certains de nombre de protections est **absolument nécessaire**.

### Masque de protection

Indispensable, non négociable.

On oublie les masque médicaux, absolument inefficace pour se protéger des particules de peinture qui peuvent circuler dans l'air, même avec une cabine de peinture et une bonne aération.

On va utiliser des masque à cartouche, que l'on trouve très facilement en magasin de bricolage. Il existe plusieurs niveau de p1 à P3, du moins vers le plus protecteur. Dans l'utilisation qu'on va en faire, le niveaux P2 au minimum est absolument nécessaire.

>:warning: : Les cartouches pour masque sont livré dans un contenant hermétique. Dès l'ouverture de celui-ci la cartouche a une durée de vie de 6mois en moyenne. Pour plus de détails, consultes la documentation sur l'emballage.

>:ballot_box_with_check: : Il est possible de se passer de masque __**uniquement, et uniquement seulement**__  dans ce cas : quand l'on travaille avec une cabine ayant une aspiration de 240㎥/h.

### Lunettes

Indispensable, non négociable. 

Exactement le contenu du point précédent.

### Gants

Des gants en latex de préférences. Trouvables en pharmacies. Et idéalement du talc. 

Des alternatives pour les personnes allergiques au latex existe, mais il est préférable de vérifier si ils sont résistant aux différents produit utilisé (principalement diluant et produit de nettoyage).

Moins indispensable que les équipement précédent, mais très fortement recommandé. Certains des produits utilisés sont agressif pour la peau, et d'autre nocif si il passe dans le corps via des plaies récentes. Sans compter le fait de retrouver des mains propres juste en les enlevant.

Se badigeonner les mains de talc avant de les enfiler et beaucoup plus confortable pour de longue session de painture.

éventuellement, comme moi, utilisé des gants noir de tatoueur pour avoir l'air inutilement cool.

## Matériel pour commencer à peindre

### Quelle aérographe ?

>NT;DR : aérographe double action par gravité avec une aiguille de 0.2 ou 0.3 De préférences le moins chère possible.

Il existe énormément aérographe sur le marché, de plus ou moins grandes marques, de plus ou moins bonne facture, avec diverse option... Entre deux aérographe de marque A et B, disposant des même options, le fonctionnement est strictement identique.

#### options

- Simple ou double action :
  - Par **simple action** : la pression exercée sur la gâchette va contrôler simultanément le flux d'air et celui de peinture.
  - Par **double action** : La gâchette a deux degrés de liberté. En pressant la gâchette on gère le débit d'air.
- Gravité ou Aspiration :
  - Par **Gravité** : Le godet contenant la peinture est placé en haut de l'aérographe. La peinture tombe dans celui-ci.
  - Par **Aspiration** : Le godet se trouve en dessous de l'aérographe. La peinture est aspiré par le flux d'air. Dans cette configuration le double-action ne peux pas exister.
- Taille d'aiguille :
  - **0.15** : Pour les travaux de très grande précision. Généralement inutile en gunpla.
  - **0.2** : Pour les travaux de précision.
  - **0.3** : Pour les travaux "normaux".
  - **0.5** : Pour les travaux qui nécessité un spray large pour couvrir une grande zone. Totalement inutile en gunpla.
- **Vis de réglage de pression** : Option pas nécessairement présente sur tout les aérographe possède une visse de réglage de pression, très souvent en bas du godet. Très pratique elle permet de diminuer la pression venant du compresseur, sans devoir aller sous le bureau pour régler son régulateur.
- ... d'autres options exotiques ou accessoires qui ne nous intéressent pas ici...

#### Premier aérographe : Grande marque ? Premier prix ?

> cf avant propos, premier point. Pour ta propre sécurité....

Rien ne justifie l'achat d'un aérographe haut de gamme... Même pas pour un professionnel et encore moins pour un premier aérographe.

Le seul critères essentiel pour ce premier aérographe est le prix. Même pas la peine de savoir si les pièces de rechange sont facilement disponibles ! Ce premier aéro il est déjà détruit, mais il ne le sait pas encore. Pour apprendre à l'utiliser (et comment le détruire), un aérographe à 20-30€ est plus que suffisant. Et ensuite, une fois un à plusieurs aéro low cost totalement saccagé, que t'arrives à te sentir à l'aise avec l'outils, là on pourra passer sur du moyen gamme qui sera plus que suffisant même à un utilisateur confirmé.

>Beaucoup de gens vont recommander des aérographe haut de gamme, partant du principe que c'est un investissement dans le temps. Maus cela peut vite finir par "des dépenses en plus dans le temp"...
>
>Disons les termes...
>
>Il n'y a aucun aérographe qui rendra ton niveau de peinture spectaculaire juste parce que ton aérographe est un iwata custom micron à plus de 500euro. Les aérographe haut de gammes n'ont **strictement aucun** effet sur le rendu final. Entre un stylo bille de marque Bic à 60cents et un autre de marque MontBlanc à 300€, y'en a-t-il un qui te faire écrire du Maitre Grims et l'autre du Victor hugo ?
>
>A l'usage, un premier aérographe il va souffrir. Mauvais entretien, aiguille abimé, joint pas foutu correctement, jamais graissé... et tant d'autre qui transformerons un aéro haut de gamme en moyen gamme, et cela sans que le débutant s'en rendre compte... 
>
>Les seules différences entre haut et moyen gamme sont les matériaux et le confort de certaines manipulation, qui sont toute autres que peindre. Apparement cela ne justifie pas l'achat pour les plus grand builder professionnel ou les vainqueurs de concours ou de compétition...

### Quelle cabine de peinture ?

//TODO démerdez vous. J'ai fabriquer la mienne.

### Quel compresseur ?

//TODO putain j'y connais rien....

### Support, pinces crocodiles

Le grattoir pour chat... Juste pour l'image général. Acheter des support dédié au maquettisme, ou effectivement un grattoir pour chat, sont des option possible, mais une alternatives tout aussi efficaces existe.

> Faire son propre support en trois étapes :
>
> 1. trouver du carton ni trop fin ni trop épais. Couper des morceaux d'au moins 25cm de long et 5cm de haut. Entourer le nombre voulu de bout de tranche, fermement avec du duct tape. Couvrir une des 2 autres faces de duct tape pour créer un fond.
> 1. ???
> 1. T'as un support.

Des pinces crocodiles pour tenir les pièces. Surprenant, non ? La pince crocodile est là pour ne jamais avoir à toucher la pièces directement. A accrocher à un point non visible de la pièce une fois monté, par exemple un ergot épais, tout aussi sans surprise.

Parfois il n'y a pas de point d'accroche. Mais un trou à ergots par exemple, il est possible d'y faire passer un pique à brochette, en coupant le côté pointu pour atteindre le diamètre voulu.

Il existe le cas "Je sais absolument comment faire". Pas de soucis, un bout de plaplat d'environ 2-3cm par côté, le tout recouvert de scotch double face, le tout sur une pince crocodile, fera le job. Seul contrainte, la pince crocodile doit supporter le poids ce montage.

Les pinces crocodiles c'est souvent le genre de matériel dont on en a jamais assez. T'as acheté un sachet de 40 pince.... Pauvres fou, ce n'est toujours pas assez ! Par exemple on peut vouloir peindre en plusieurs session un gros MG, mais au milieu se détendre en vernissant un HG pour du panel line... 80 pinces croco semble un bon début.

### Autres outils utiles

- Un pot en verre pour aérographe. Sa spécificité et d'avoir un couvercle ouvert permettant de spray le contenu du godet dans ce pot proprement.
- Des pipettes en plastique pour mesurer à peu près la quantité les quantités de liquide à placer dans le godet.
- Un contenant.

## Préparer ses pièces avant de peindre

Avant de sous coucher et peindre ses pièces il indispensable de les nettoyer : les pièces étant couverte de graisse de démoulage, et de la graisse de Doritos qu'il reste sur tes doigts.

### Nettoyage à la main

Dans un récipient type bol, voire saladier, rempli d'un fond de liquide vaisselle et d'eau, on place no pièces, On touilles un peu, on laisse reposer 30 min. On remplace l'eau savonneuse du bol par de l'eau.

A l'aide d'une brosse à dent, on frotte sa pièce pour retirer ce qu'il reste de savon, et la dépose sur du sopalin. Pour chaque pièces...

On laisse sécher à l'air libre pendant 24h.

### Nettoyage express

En plus d'être plus rapide, cette méthode est aussi plus efficace.

Dans un bac à ultrason rempli d'un fond de liquide vaisselle et d'eau, programmé sur 10 minutes. Pendant ce temps là, on fait tout ce qu'il est possible de faire en 10 min.

On retire du bac, oet remplace l'eau savonneuse par de l'eau propre puis on remet les pièces dans le bac, programmé sur 10 minutes. Pendant ce temps là, on fait tout ce qu'il est possible de faire en 10 min.

On retire tout du bac et l'on dépose sur du sopalin. 

On sèche chaque pièces une par une avec un sèche cheveux en mode "cold".

## La sous couche

La sous couche permet de neutraliser la couleur d'une pièce et de permettre à la peinture d'accrocher sur la pièce. 

Ce n'est pas une peinture comme les autres car elle accroche sur le plastique. une peinture grise par exemple ne remplacera pas une peinture de sous couche.

### couleur de sous couche

- Gris : le cas général neutralise le niveau d'une couleur. La couleur de la peinture sur la pièce sera celle de la peinture elle même.
- Noir : une peinture posé sur cette sous couche deviendra plus foncé. Généralement utilisé pour un preshading.
- Blanc : une peinture posé sur cette sous couche deviendra plus claire. Généralement utilisé pour un prelighting.
- Les couleur chaire : différentes teintes de peaux. Utilisé pour le custom de musume, permettant au builder de se concentré sur les détails plutôt que sur la peau.
- autres : plusieurs teinte de couleur utilisé pour des cas très particulier. Le rouge rouille pour des modèle militaire, ...

> La sous grise est d'un gris parfaitement neutre. Mélanger du blanc et du noir ne permettra pas d'avoir un gris parfaitement neutre, il sera plus foncé ou plus claire, et donc perdra intérêt.

### Type de sous couche

- Primer : une sous couche pour kit qui n'a pas été customisé, à utilisé sur le plastique tel que sortie d'usine.
- Surfacer : Utilisé après custom, particulièrement si les pièces ont été poncé, et donc inévitablement avec des surface de grain différent. Ce type de sous couche uniformise le grain de chaque pièce. Un surfacer 1500 donnera un grain uniforme de 1500.

### Bombe ou aérographe

La bombe ne demande aucune préparation ni aucune dilution. Rapide, efficace.

La sous couche à l'aérographe est utile si l'on a pas d'espace et d'aération de disponible. La sous couche pour aérographe nécessite d'être dilué, et ceci plus qu'une peinture "normal".

## La dilution

Les peintures sont trop épaisse pour être utiliser tels quel dans un aérographe. Par définition donc, la dilution consiste à mélanger la peinture et le diluant. Il est indispensable de diluer.

### Les type de peintures et de diluant

Il existe 3 type de peinture avec **leur diluant spécifique** :

1. Lacquer
2. Acrylique
3. Enamel

Les diluant pur peuvent diluer la peinture de ce type et tout ce qui se trouve en dessous dans la liste ci dessus. Par exemple le diluant lacquer peut "effacer" du lacquer, de l'acrylique et de l'enamel. Un diluant enamel n'effacer" que lui même. Malgré cela il est préférable d'utiliser le diluant adapté.

C'est pour cette raison que l'enamel est traditionnellement utilisé pour nettoyer le panel line, son diluant n'effaçant pas les couches de peinture en dessous.

Chaque fabriquant de peinture propose son diluant, supposément le mieux adapté à ses peinture.

> Additif
>
> des additif pour diluant existent pour modifier le comportement du diluant. Par exemple un retardateur qui permettra au diluant de sécher plus lentement, utile pour faire certains types dégradé. Certains fabriquant propose des bouteilles avec additif pré-mélanger.

### Ratio peinture:diluant

Il est impossible de donner un ratio exacte universelle. La dilution dépend d'énormément de facteur : le pigment de la peinture, la pression du compresseur utilisé, l'humidité ambiante,...  Les recommandations des fabricant ne sont que de l'à peu prêt.

La seul règle absolue : le mélange doit avoir une consistance laiteuse. Ce principe de dilution est incroyablement difficile à expliquer, sinon impossible. La seule méthode pour comprendre globalement l'idée par soit même est relativement simple : prendre une feuille blanche, dilué à 1:1, donner un coup de spray, rajouter un goute de diluant, spray, rajouter une goutte de diluant, spray, et ainsi de suite, jusqu'à comprendre ce que signifie être trop dilué : si le spray montre clairement que y'a beaucoup plus de diluant que de pigment. Pour comprendre si la peinture est pas assez dilué, cela se fera plus par accident, allant d'un flux de peinture très faible jusqu'à boucher l'aérographe.

### Ratio recommandé par marque par marques

Les ratio ici seront décris sous la forme 1:2, 1 étant la peinture, 2 le diluant.
La liste de fabriquant est non exhaustive.

#### Vallejo

Dans certains le fabricant recommande d'utilise son airbrush flow improver (71.262).

- Gamme "Air" : pré-dilué, si nécessaire ajouter une goutte ou deux goute de flow improver.
- Gamme normal : 1:1~2, si nécessaire rajouter du flow improver en petite quantité.

#### Mr Color / Mr hobby / CGI Creos

CGI  propose 4 Thinner 

1. Mr Thinner : le retardant de base de la marque.
1. Mr Leveling Thinner : un diluant très légèrement retardé.
1, Mr Rapid Thinner : diluant à séchage rapide.
1. Mr Retarder Mild (uniquement sous forme d'additif) : diluant à séchage lent.

Le ratio de base est 1:1~2 (pour Thinner et Leveling Thinner). Le Leveling thinner est recommandé pour la gamme GX et les peintures métallique. Le Leveling thinner est le plus polyvalent. 

#### Citadel

- Gamme Air series : pré-dilué, prêt à être utilisé.
- gamme normal : 1:1~2, diluant Vallejo préconisé.

La gamme Technical est à proscrire.

#### Gaia Notes

Le fabricant proposent différent diluant :
- Thinner : diluant de base, 1:1~2.
- Rapid Thinner : séchage rapide, 1:1~2.
- Metallic Thinner : pour les peinture métalliques, 1:1~2.


### Je suis une quiche, j'ai bouché mon aéro

Une seule solution : nettoyage complet de complet de l'aéro.

## Le cas de peinture métallique

Les couleurs métalliques sont exclusivement à poser sur du gloss block, sans cela l'aspect métallique ne ressortira pas du tout.

Un sous couche noir, non gloss, ne sera pas suffisantes. Il sera donc nécessaire de reposer une couche de peinture gloss black.

Certains fabriquant proposent des sous couches gloss black.

Les peintures métalliques requiers souvent une dilution plus forte.

>:warning: Nettoyage
>
>Au moment de changer de couleur, si la couleur suivante est aussi une peinture métallique, un nettoyage simple est suffisant. Par contre, si la couleur suivante est non métallique un nettoyage complet est nécessaire pour nettoyer les paillettes métalliques résiduelles  dans l'aérographe.

## Le vernis


## Le Backwash

## Astuce:   recyclage du diluant
